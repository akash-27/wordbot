const natural = require('natural');
const express = require('express');
const cors = require('cors');
const req = require('express/lib/request');
const wordnet = new natural.WordNet();
const app = express();
const port = 3000;
app.use(cors());
app.use(express.static(__dirname + '\\public'));
var wordDict = {};
app.get("/", (request, response) => {
    response.sendFile(__dirname + "\\index.html");
});
app.get("/wordbot/:word", (request, response) => {
    if(wordDict[request.params.word] == undefined){
        wordnet.lookup(request.params.word, function(results) {
            var wordMeaning = {};
            if(results[0] == undefined) {
                response.send(["Oops! I couldn't find an answer to this. Try again!"]);
            }
            else{
            wordMeaning.definition = results[0].def;
            wordMeaning.synonyms = results[0].synonyms;
            wordDict[request.params.word] = wordMeaning;
            console.log('word sent after lookup');
            response.send(wordMeaning);
            }
        });
    }
    else {
        console.log('word sent from local dictionary');
        response.send(wordDict[request.params.word])
    }
});
  
app.listen(port, () => {
  console.log(`app listening on port ${port}`);
});